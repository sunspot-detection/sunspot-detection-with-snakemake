# Automatic sunspot detection from USET images

This repository contains a workflow for the automatic detection of sunspots in images of the sun available from the [USET telescopes](https://est-east.eu/component/content/article/22-english/1088-uset-the-uccle-solar-equatorial-table) located in Brussels (Belgium). It is provided as an example of a realistic scientific workflow for the MOOC ["Reproducible Research II: Practices and tools for managing computations and data"](https://lms.fun-mooc.fr/courses/course-v1:inria+41023+session01/info).

## Getting started

The workflow uses `git-annex` to manage large files, in particular the USET images to be analyzed. This requires a specific setup for your clone of this repository:

``` shell
git clone https://gitlab.inria.fr/sunspot-detection/sunspot-detection-with-snakemake.git

cd sunspot-detection-with-snakemake

find results -exec touch '{}' +
sleep 5
touch results/sunspots/monthly*

git config annex.private true
git config remote.origin.annex-readonly true
git config remote.origin.annex-ignore true

git annex init
```

The reason for updating some file modification dates using `touch` is that git does not store modification dates. If you run `snakemake` from a repository without updating the modification times of the results, it will recompute everything, including the results that we have provided in this repository to reduce the runtime for some of the MOOC exercises.

Here is what the git configuration settings do:
* `annex.private` hides the annex of your clone from the upstream repository and from other clones. This is not strictly necessary in our case, because you do not have write access to the upstream repository anyway, but it is good practice for a local checkout meant for experimentation to make its annex private. See [this page](https://git-annex.branchable.com/tips/cloning_a_repository_privately/) for details.
* `remote.<remote>.annex-readonly` prevents `git-annex sync` from pushing information to the remote repository's `git-annex` branch. See the [git-annex manual](https://git-annex.branchable.com/git-annex/) for details.
* `remote.<remote>.annex-ignore` tells `git-annex` not to store annexed files on the upstream repository. It is kept on a GitLab instance that does not use `git-annex`, meaning that storing files there is impossible. See the [git-annex manual](https://git-annex.branchable.com/git-annex/) for details.

The last configuration command, `git annex init`, adds a local branch `git-annex` to your checkout. This branch is used by `git annex` to manage the local copies of the images processed by our workflow. This command requires `git-annex` to be installed on your computer. If it is not installed, you can run it from the same containers that will be used to run the workflow. For the three containerization tools we propose, the command lines are:

- ``` shell
  # In case git annex is not available and git annex init fails, you may use one of these commands:
  ./tools/guix_shell.sh git annex init
  ./tools/singularity_exec.sh git annex init
  ./tools/docker_run.sh git annex init
  ```
Docker requires a prior load step, as explained below in the section on Docker.


## Getting original USET files

The original observations (FITS files) are available on the [Brussels observatory servers](https://www.sidc.be/DATA/uset/Wlight/) but have also been mirrored for the MOOC on [http://mooc-rr2.inria.fr](http://mooc-rr2.inria.fr). The configuration of our repository allows downloading data from both locations, with a higher priority given to the mirror, meaning that the original site will be used only if the mirror cannot be reached.

To retrieve the images, you should use `git annex`, for example as follows:

``` shell
# Test git-annex get
git annex get uset_data/fits_images/2002/05/UPH20020502111048.FTS
```

This downloads the file (to the `.git/annex` directory), so that the symbolic link is not dangling anymore.

Finally, note that you may ask `git-annex` where the files are as follows:

``` shell
git annex whereis uset_data/fits_images/2002/05/UPH20020502111048.FTS
```

``` example
whereis uset_data/fits_images/2002/05/UPH20020502111048.FTS (2 copies)
  	00000000-0000-0000-0000-000000000001 -- web
   	9e4126ca-dd27-4bc1-b951-f91c218f7909 -- user@host:~/workspace/mooc-rr/sunspot-detection/sunspot-detection-with-snakemake [here]

  web: http://mooc-rr2.inria.fr/DATA/uset/Wlight/2002/05/UPH20020502111048.FTS
  web: https://www.sidc.be/DATA/uset/Wlight/2002/05/UPH20020502111048.FTS
ok
```

This file appears to be available on both web servers (the Brussel Observatory
web server https://www.sidc.be and our mirror http://mooc-rr2.inria.fr) and it is also available locally (`[here]`).

Actually, we have set configured these two remotes as `fasthost` (for the mirror) and `slowhost` (for the original server) with costs that indicate `git-annex` to use the mirror in priority.
``` shell
git annex list uset_data/fits_images/2002/05/UPH20020502111048.FTS
```

``` example
here
|origin
||fasthost
|||slowhost
||||web
|||||bittorrent
||||||
X_XXX_ uset_data/fits_images/2002/05/UPH20020502111048.FTS
```

## How is this project organized ?

This project is organized following the [coderefinery](https://coderefinery.github.io/reproducible-research/organizing-projects/#directory-structure-for-projects) structure.

```
sunspot-detection-with-snakemake
├── doc
├── uset_data
├── processed_data
├── results
├── workflow
└── tools
```

The `doc` directory comprises some documentation of the workflow (i.e., graphical representations of the SnakeMake workflow that help understanding how the different scripts relate to each others) as well as a journal detailing the commands which have been run to set up this repository (in particular the use and the configuration of `git-annex`).

All the input data (i.e. FITS images provided by USET) live in the `uset_data/fits_images` folder. They have been annexed using the  [`tools/list_available_images.py`](tools/list_available_images.py) python script.
Refer to [tools](./tools/README.md) for understanding how to update the list of available images.

All intermediate data produced during workflow execution are stored in the `processed_data` folder.

The results of our analysis are stored in `results` folder.

Finally, the code for the analysis and the workflow itself are stored in the `workflow` folder.

## How to launch our analysis workflow ?

We have used `snakemake` for defining our workflow for analyzing the data.
This workflow automatically calls `git-annex get` to download data when
needed (i.e., if it is not present in your `uset_data` folder).
We assume that the workflow is run in a software environment that provide the
tools and libraries the scripts need (`pandas`, `fitsio`, `matplotlib`, etc.).

We provide several examples below, using either Guix, singularity, or docker, always
assuming that commands are run from the **top directory** of the project.

### Using `snakemake` with Guix

To run snakemake within a Guix environment comprising `git-annex`, `snakemake`, `python-scikit-image`, and `python-fitsio`, proceed as follows:

```
./tools/guix_shell.sh snakemake --snakefile workflow/Snakefile --cores 3 results/sunspots/2002/05/UPH20020502111048_list.json
```

The script `guix_shell.sh` incorporates all the command line options required to run our workflow. We encourage you to look at it, because it also contains explanations of the options that we have not discussed in the MOOC.

The command is likely to take some time on the first execution as it will require downloading or compiling several software packages.

### Using `snakemake` with Singularity

First download the image file `sunspots-singularity.sif` from http://mooc-rr2.inria.fr/DATA/container-images/sunspots-singularity.sif to the directory `./container-images` in the top directory of the project. If you have Guix installed on your computer (or on some other computer you have access to), you can also create this file yourself, reproducibly, by running the script `tools/make-singularity-image.sh`. The workflow can then be run within singularity as follows:

```
./tools/singularity_exec.sh snakemake --snakefile workflow/Snakefile --cores 3 results/sunspots/2002/05/UPH20020502111048_list.json
```

The script `singularity_exec.sh` incorporates all the command line options required to run our workflow. We encourage you to look at it, because it also contains explanations of the options that we have not discussed in the MOOC.

### Using `snakemake` with Docker

Like Singularity, Docker requires an image file that you can either construct yourself, reproducibly, via Guix and the script `tools/make-docker-image.sh`, or download from
http://mooc-rr2.inria.fr/DATA/container-images/sunspots-docker.tar.gz
and place into the directory `container-images`.

Next, you must load the file before you can use it (this can take a while):

```
docker load -i container-images/sunspots-docker.tar.gz
```

Once loaded, you can remove `container-images/sunspots-docker.tar.gz` and you can run commands from docker as often as you like:

```
./tools/docker_run.sh snakemake --snakefile workflow/Snakefile --cores 3 results/sunspots/2002/05/UPH20020502111048_list.json
```

The script `docker_run.sh` incorporates all the command line options required to run our workflow. We encourage you to look at it, because it also contains explanations of the options that we have not discussed in the MOOC.

Finally, once you have finished working with the workflow, do not forget to remove the loaded image:
```
docker image rm snakemake-git-git-annex-bash-coreutils:latest
```

### Main rules of interest

`snakemake` will require you to indicate how many cores to use, which is why we always run it with the `--cores 3` option in our examples but feel free to adapt this to your own machine.

All the previous examples have used a single file (`results/sunspots/2002/05/UPH20020502111048_list.json`) as a target but many targets are available. Here are the main ones, by order of computational complexity.

- `results/sunspots/{year}/{month}/{name}_list.json` is a target of the `sunspot_detection` rule. It will start from the corresponding `FITS` file (`uset_data/fits_images/{year}/{month}/{name}.FTS`) and apply (1) the `workflow/scripts/preprocess_image.py` script to identify the solar disk and (2) the `workflow/scripts/detect_sunspots.py` to detect the sunspots. The sunspot locations in the image are available from the `json` file along many other information. The input and output files of this process are illustrated  [here](doc/filegraph_sunspot_detection_for_a_single_file.pdf).

- `sunspot_detection_on_random_sample` will pick 5 images are random in the `uset_data/fits_images/` directory and apply the `sunspot_detection` on them. It is an interesting sanity check of the `--core 3` option of `snakemake` and of the downloading capability of the workflow. Here is how to run it through `singularity`:

   ```
   singularity exec --bind `pwd`:/project/ --pwd /project container-images/sunspots-singularity.sif snakemake --snakefile workflow/Snakefile --cores 3 sunspot_detection_on_random_sample
   ```

- `monthly_mean_2009_to_2020` will select an image every three days in the year range from 2009 to 2020, apply the `sunspot_detection` rule, and compute a time series containing one point per month as an average over all the images that fall into the month. It produces a text file containing the monthly averages plus an image file showing a plot in comparison with the official monthly timeseries from the the Brussel Observatory web server. This workflow is illustrated [here](doc/filegraph_monthly_mean_2009_to_2020.pdf).

Since we use the output of this rule in an exercise, we have run this rule and commited the sunspot count files and the monthly average files to this repository. This means that nothing will be computed if you try to re-run thus rule. Snakemake will simply tell you that all files are up to date. You can remove some of these files and see what happens if you run the rule again. You can also change the starting or ending year to make the interval longer. Running the rule should then perform sunspot detection on some additional images.

- `sunspot_detection_all` will apply the `sunspot_detection` rule on all available images. This will take a very long time to complete and you obviously do not want to run this rule ! This workflow is illustrated [here](doc/filegraph_sunspot_detection_all.pdf).

### Modifying or adding your own rules

Feel free to experiment with this workflow by modifying the rules we provide (e.g. change the year range for the monthly average time series) or by adding scripts and rules for your own analyses. The Snakefile is heavily documented to help you with such projects.

Note in particular that the rules `preprocess_image` and `sunspot_detection` may fail for some images in the USET collection, because some images are very different from the expectations of our scripts. For example, some images are completely black. If a script fails, Snakemake aborts the execution of the workflow. You can ask it to continue nevertheless (the option is `--keep-going`), but any rule that depends on processed data for such an image will fail nevertheless. For this reason, we have provided a mechanism to exclude specific images from the database, such that they will never even be considered as inputs, unless you ask specifically for them by name. Check the file `workflow/bad_images.txt` and comments referring to it in `Snakefile`. If you encounter another bad file, make sure you understand what causes the failure, and if the file is really a bad one, add it to the exclusion list.

### License

This project is licensed under GPLv3 license.
It is used as materials for the MOOC Reproducible research 2.

Concerning input data (i.e. `uset_data` folder), they are publicly available on
the following website: [https://www.sidc.be/uset](https://www.sidc.be/uset).

**Sources**:

- USET data/image, Royal Observatory of Belgium, Brussels
  - web link : https://www.sidc.be/DATA/uset/Wlight/
  - data policy : https://www.astro.oma.be/common/internet/en/data-policy-en.pdf
- WDC-SILSO, Royal Observatory of Belgium, Brussels
  - web link : https://www.sidc.be/silso/DATA
  - data policy : [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)
