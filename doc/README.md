These graphs have been generated using these commands at root of the project:

Note that the graphs are not generated as part of the workflow. They may become outdated. If you make any change whatsoever to the workflow, you have to re-generate the graphs manually!

```
guix shell -C -N --expose=/etc/ssl/certs --expose=/etc/protocols -m workflow/manifest.scm -- snakemake --snakefile workflow/Snakefile --cores 3 --filegraph results/sunspots/2002/05/UPH20020502111048_list.json | dot -Tpdf > doc/filegraph_sunspot_detection_for_a_single_file.pdf

guix shell -C -N --expose=/etc/ssl/certs --expose=/etc/protocols -m workflow/manifest.scm -- snakemake --snakefile workflow/Snakefile --cores 3 --filegraph intensity_statistics_on_random_sample | dot -Tpdf > doc/filegraph_intensity_statistics_on_random_sample.pdf

guix shell -C -N --expose=/etc/ssl/certs --expose=/etc/protocols -m workflow/manifest.scm -- snakemake --snakefile workflow/Snakefile --cores 3 --filegraph sunspot_detection_all | dot -Tpdf > doc/filegraph_sunspot_detection_all.pdf

guix shell -C -N --expose=/etc/ssl/certs --expose=/etc/protocols -m workflow/manifest.scm -- snakemake --snakefile workflow/Snakefile --cores 3 --filegraph monthly_mean_2009_to_2020 | dot -Tpdf > doc/filegraph_monthly_mean_2009_to_2020.pdf
```
