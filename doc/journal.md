# Journal

## 2023-12-07 - Using git-annex for adding uset images

For annexing images from 2002
```
python3 tools/list_available_images.py --url https://www.sidc.be/DATA/uset/Wlight -y 2002 2002 2002_available_images.txt
cat 2002_available_images.txt | git annex addurl --batch --with-files --jobs=4 --pathdepth=-3 -c annex.commitmessage='Annex images from year 2002'
git commit -m "Annex images from year 2002"
git push origin main git-annex
```

Adding relaxed web url (mirror) for one image

```
git annex addurl --relaxed --file=uset_data/fits_images/2002/05/UPH20020502111048.FTS  http://mooc-rr2.inria.fr/DATA/uset/Wlight/2002/05/UPH20020502111048.FTS -c annex.commitmessage='add relaxed url (mirror) for one image'
git push origin git-annex
```

## 2023-11-21 - Initialization of the gitlab repository

The source code has been copied from the last status 15-11-2023 at
`git@gitlab.inria.fr:msimonin/sunspot-exploration.git` :
- commit `ed752c5af574052995688eeec1790d2e382dc367`

Then, it has been modified with a new project organization for proposal.

For being shared on [gitlab](https://gitlab.inria.fr/sunspot-detection/sunspot-detection-with-snakemake) these commands have been executed :

```
cd sunspot-detection
git remote add origin git@gitlab.inria.fr:sunspot-detection/sunspot-detection-with-snakemake.git
git config -e
#### Add these lines
[annex]
  private = true
  autocommit = false
  largefiles = "include=*.FTS"
  resolvemerge = false
[remote "origin"]
  annex-readonly = true
  annex-ignore = true
####
git-annex init -c annex.commitmessage="Init git-annex"
git annex config --set annex.autocommit false -c annex.alwayscommit=false
git annex config --set annex.largefiles "include=*.FTS" -c annex.alwayscommit=false
git annex config --set annex.resolvemerge false -c annex.alwayscommit=false
git annex sync -c annex.commitmessage="Configure git-annex"
git push origin git-annex
```

## 2023-12-21 - Setting up a mirror
All the files from https://www.sidc.be/DATA/uset/Wlight/ have been mirrored for the MOOC on http://mooc-rr2.inria.fr/DATA/uset/Wlight/. Let's tell git-annex we have alternate download URLs:

```
cd uset_data/fits_images/
find ./ -name '*.FTS' | sed 's|^./|http://mooc-rr2.inria.fr/DATA/uset/Wlight/|' | git-annex addurl --batch --fast --relaxed --raw --pathdepth=-3 --jobs=4 -c annex.commitmessage='Setting up a mirror'
```

Now, let's indicate that the Inria mirror should be preferred over the original server by indicating it has a lower cost (the default cost for a web remote is 200).
```
git-annex initremote --sameas=web fasthost type=web urlinclude='*//mooc-rr2.inria.fr/*' cost=150
git-annex initremote --sameas=web slowhost type=web urlinclude='*//www.sidc.be/*' cost=300
git-annex list
```
And finally
```
git push origin git-annex
```

## 2023-12-22 - Running the workflow on Grid5000

Mathieu Simonin had asked for a shared storage group in Nancy (`/srv/storage/moocrr2@storage1.nancy.grid5000.fr/`) where the repository has been cloned following the *Getting Started* instructions.

```
ssh nancy.g5k
cd /srv/storage/moocrr2@storage1.nancy.grid5000.fr/
git clone git@gitlab.inria.fr:sunspot-detection/sunspot-detection-with-snakemake.git
cd sunspot-detection-with-snakemake
git config annex.private true
git config remote.origin.annex-readonly true
git config remote.origin.annex-ignore true
mkdir container-images/
cd container-images/
wget http://mooc-rr2.inria.fr/DATA/container-images/sunspots-singularity.sif
```

The `fasthost` and `slowhost` remotes should be activated with `git-annex enableremote fasthost` and `git-annex enableremote slowhost` but `git-annex` is not available in the front-end. Instead, I had to run oarsub, module load singularity, and run git-annex from singularity as shown below.

Once the repository is set up, we can start computing things, e.g.,:

```
ssh nancy.g5k
oarsub -I -l walltime=1
cd /srv/storage/moocrr2@storage1.nancy.grid5000.fr/sunspot-detection-with-snakemake
module load singularity
alias singrun='singularity exec --bind `pwd`:/project/ --pwd /project --bind /etc/ssl/certs:/etc/ssl/certs --bind /etc/protocols:/etc/protocols container-images/sunspots-singularity.sif'
# singrun git-annex enableremote fasthost
singrun git-annex get uset_data/fits_images/2002/05/UPH20020502111048.FTS
singrun snakemake --snakefile workflow/Snakefile --cores 3 results/sunspots/2002/05/UPH20020502111048_list.json
singrun snakemake --snakefile workflow/Snakefile --cores 32 sunspot_detection_on_random_sample
```

I could even run the whole `monthly_mean_2009_to_2020` rule in less than 2 hours a single [`grisou` node](https://www.grid5000.fr/w/Nancy:Hardware#grisou) (2 Intel Xeon E5-2630 CPUs with 8 cores/CPU and hyperthreading enabled, hence 32 logical cores available):

```
singrun snakemake --snakefile workflow/Snakefile --cores 40 monthly_mean_2009_to_2020
```

```
Building DAG of jobs...
Using shell: /gnu/store/nb9viq1czip2plysz1c82yq4r565q403-profile/bin/bash
Provided cores: 3
Rules claiming more threads will be scaled down.
Job stats:
job                          count    min threads    max threads
-------------------------  -------  -------------  -------------
download                      1434              1              1
monthly_mean_2009_to_2020        1              1              1
preprocess_image              1434              1              1
sunspot_detection             1434              1              1
total                         4303              1              1
```

2020 to 2009 represents 12 years, hence 144 months, hence about 1440 images to process. That's coherent.

In the end 12GB of FTS files out of 495 were downloaded:

```
singrun snakemake --snakefile workflow/Snakefile --cores 40 monthly_mean_2009_to_2020
```

```
trusted repositories: 0
semitrusted repositories: 4
	00000000-0000-0000-0000-000000000001 -- web
 	00000000-0000-0000-0000-000000000002 -- bittorrent
 	5383b123-f932-4655-ae8c-93bdbe7e386a -- here
 	90b3ed88-110a-4842-9e79-9722bd0a6684 -- alegrand@icarus:~/Work/Documents/Enseignements/RR_MOOC/sunspot-detection-with-snakemake
untrusted repositories: 0
transfers in progress: none
available local disk space: 495.67 gigabytes (+100 megabytes reserved)
local annex keys: 1434
local annex size: 12.03 gigabytes
annexed files in working tree: 34482
size of annexed files in working tree: 258.76 gigabytes
bloom filter size: 32 mebibytes (0.3% full)
backend usage: 
	SHA256E: 34482
```

I could then commit the json and log files as follows:
```
singrun git add `find results/ -name '*.json'`
singrun git add `find results/ -name '*.log'`
singrun git add -f results/sunspots/monthly_sunspot-numbers_2009-2020.png uset_data/sunspots/SN_m_tot_V2.0.txt
```

