# Some tools

Tools must be used at project root, i.e., `./tools/<script_to_execute>`.

## Listing and annexing images

* Help

```
python3 tools/list_available_images.py -h
```

* Listing files to annex (for year 2002 on uset server) ->
  2002-available-images.txt

```
python3 tools/list_available_images.py --url https://www.sidc.be/DATA/uset/Wlight -y 2002 2002 2002_available_images.txt
```

The default behavior is provided in script help.

* Annex files

To annex files, we are using the text file as input for git-annex addurl and
provide a commit message for git-annex branch.
Once files are annexed (committed in `git-annex` branch), we also have to commit
the symbolic links in `main` branch.
Finally, both branches are pushed : `git-annex` and `main`.

Example:

```
cat 2002_available_images.txt | git annex addurl --batch --with-files --jobs=4 --pathdepth=-3 -c annex.commitmessage='Annex images from year 2002'
git commit -m "Annex images from year 2002"
git push origin main git-annex
```

Note: If you try to reannex files that have already been annexed on the same
path, it does nothing (i.e, if you relaunch the same `git-annex addurl` command,
there is no effect).


* add mirror url

TODO: to complete with priority (cf.)

Mirror url: http://mooc-rr2.inria.fr/DATA/uset/Wlight

## Creating container images

If you have Guix installed on your computer (or on some other computer you have access to), you can create the Singularity image yourself, reproducibly, by running the script :

```
./tools/make-singularity-image.sh
```

Like Singularity, Docker requires an image file that you can either construct yourself, reproducibly, via Guix and the script `tools/make-docker-image.sh`

## Executing workflow in containers

The scripts `guix_shell.sh`, `singularity_exec`, and `docker_run.sh` are provided for executing the workflow in containers without typing lengthy command lines.

You must be at the project root for launching these scripts, because the project is mounted inside the container.

Example:

```
./tools/docker_run.sh snakemake --snakefile workflow/Snakefile --cores 3 sunspot_detection_on_random_sample
```

