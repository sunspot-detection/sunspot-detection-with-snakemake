#!/bin/sh

script=$0

usage() {
  echo "Usage: $script CMD"
  echo
  echo "Run CMD in a docker container from the image named"
  echo "'snakemake-git-git-annex-bash-coreutils:latest'. The image"
  echo "must have been loaded before."
  echo
  echo "Example: '$script ls' runs the 'ls' command in docker container"
}

# check that a command is provided
if [ $# -lt 1 ]
then
    usage
    exit 1
fi

echo $@

docker run --name sunspot-detection-env \
  -u $(id -u):$(id -g) --rm \
  -e XDG_CACHE_HOME=/tmp/.cache \
  -e MPLCONFIGDIR=/tmp \
  -v $(pwd):/tmp -w /tmp \
  snakemake-git-git-annex-bash-coreutils:latest \
  "$@"

# Explanations:
#
# - Launching docker with current user and group ids (-u option) allows to write
#   files with same rights as the user account on the host system
#
# - XDG_CACHE_HOME must be set because it is used by snakemake
#   (cf. https://snakemake.readthedocs.io/en/stable/executing/cli.html#important-environment-variables)
