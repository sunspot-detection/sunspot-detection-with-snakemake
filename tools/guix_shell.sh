#!/bin/sh

script=$0

usage() {
  echo "Usage: $script CMD"
  echo
  echo "Run CMD in a Guix container defined by workflow/channels.scm"
  echo "and workflow/manifest.scm"
  echo
  echo "Example: '$script ls' runs the 'ls' command in a Guix container"
}

# check that a command is provided
if [ $# -lt 1 ]
then
    usage
    exit 1
fi

echo $@

guix time-machine \
  -C workflow/channels.scm \
  -- \
  shell \
  --container \
  --network \
  --expose=/etc/ssl/certs \
  --expose=/etc/protocols \
  -m workflow/manifest.scm \
  -- \
  "$@"

# Explanations:
#
# - The directory /etc/ssl/certs must be made available in the container to permit
#   downloading files from https URLs.
#
# - The file /etc/protocols is shared because it is used by git-annex.
#
# If you prefer to use the official Guix channel rather than our fork, replace
#    -C workflow/channels.scm
# by
#    --commit=7b0863f07a113caef26fea13909bd97d250b629e
# which is the commit that our fork is based on.
