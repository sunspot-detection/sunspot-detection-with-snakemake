# This script is a tool for listing the images available on the web server.
#
# Note that the output of this script is *not* reproducible. Images
# are regularly added, and while image deletion should not happen in
# theory, it is a possibility that we have no control over.
#
# The file produced by this script aims to be used by git-annex for versioning the
# images used for the analysis.

import argparse
import re
import sys
import urllib.request
import urllib.error

from pathlib import Path

# Define the script arguments
parser = argparse.ArgumentParser(description="List the available images to be annexed")
parser.add_argument(
    "--url",
    nargs='?',
    default="https://www.sidc.be/DATA/uset/Wlight",
    help="url to parse (default: http://mooc-rr2.inria.fr/DATA/uset/Wlight)"
)
parser.add_argument(
    "-y",
    "--years",
    metavar=("START", "END"),
    type=int,
    nargs=2,
    default=[2002, 2022],
    help="range of included years (default: 2002 2022)",
)

parser.add_argument(
    "-l",
    "--location",
    nargs='?',
    default="uset_data",
    help="data folder where to store fits image folder (default: uset_data)"
)

parser.add_argument(
    "output",
    type=argparse.FileType('w'),
    nargs='?',
    default=(Path(__file__).resolve().parent.parent / "available_fits_images.txt"),
    help="output file (default: available_fits_images.txt)"
)
args = parser.parse_args()
years = args.years
data_folder = args.location
output = args.output.open('w') if isinstance(args.output, Path) else args.output

# The file names contain a sequence of 14 digits encoding
# date and time: yyyymmddhhmmss.
pattern = r"href=\"(UPH\d{14}\.FTS)\""


# The global list of images are written in a file that will be used to create the annex

for year in range(years[0], years[1] + 1):
    for month in range(1, 12 + 1):
        url = f"{args.url}/{year:4}/{month:02}"
        try:
            with urllib.request.urlopen(url) as s:
                html_index = s.read().decode()
                filenames = re.findall(pattern, html_index)
                for filename in filenames:
                    output.write(
                        f"{url}/{filename} {data_folder}/fits_images/{year:4}/{month:02}/{filename}\n"
                    )
        except urllib.error.HTTPError:
            print(f"No data for {month}/{year}", file=sys.stderr)

output.close()
