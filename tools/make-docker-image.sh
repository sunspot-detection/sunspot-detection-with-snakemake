#!/bin/sh
mkdir -p ./container-images
cp $(guix time-machine -C ./workflow/channels.scm -- pack -m ./workflow/manifest.scm -S /etc/ssl=etc/ssl -S /etc/protocols=etc/protocols --format=docker --save-provenance) ./container-images/sunspots-docker.tar.gz
# See https://hpc.guix.info/blog/2021/10/when-docker-images-become-fixed-point/
# for an explanation of --save-provenance
