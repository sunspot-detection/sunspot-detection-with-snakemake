#!/bin/sh
mkdir -p ./container-images
cp $(guix time-machine -C ./workflow/channels-singularity.scm --disable-authentication -- pack -m ./workflow/manifest.scm -S /etc=etc --format=squashfs --save-provenance) ./container-images/sunspots-singularity.sif

# This script uses a different channel file than direct execution of the
# workflow via Guix. See ./workflow/channels-singularity.scm  for an
# explanation.
#
# See https://hpc.guix.info/blog/2021/10/when-docker-images-become-fixed-point/
# for an explanation of --save-provenance
