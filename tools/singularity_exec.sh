#!/bin/sh

script=$0

usage() {
  echo "Usage: $script CMD"
  echo
  echo "Run CMD in a singularity container from the image stored in the file"
  echo "container-images/sunspots-singularity.sif"
  echo
  echo "Example: '$script ls' runs the 'ls' command in singularity container"
}

# check that a command is provided
if [ $# -lt 1 ]
then
    usage
    exit 1
fi

echo $@

singularity exec \
  --containall \
  --bind `pwd`:/tmp/ \
  --pwd /tmp \
  container-images/sunspots-singularity.sif \
  "$@"

# Explanations:
#
# - The option '--containall' tells Singularity not to share $HOME with
#   the container. Inside the container, $HOME is an empty directory.
#
# - The `--bind `pwd`:/project/ --pwd /project` is useless on recent
#   versions of singularity, as sharing and setting the current
#   directory as the starting point of the container is now the default.
#   It is, however, required for older versions.
#
# - This script assumes that the image file is stored at
#   container-images/sunspots-singularity.sif.
