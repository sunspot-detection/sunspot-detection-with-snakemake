This folder contains the analysis workflow described within a snakefile.

The `scripts` folder contains python scripts that are used to analyse and detect sunspots in FITS files.

- `intensity_statistics.py`: This preparatory analysis extracts information from the FITS image that is required to make good choices for the parameters in the sunspot identification. It is also helpful for debugging: if the sunspot detection returns unexpected results, look at the intensity statistics first.
The main output is an intensity histogram for each image that also shows the threshold chosen to separate the solar disk from background noise. Two additional output files contain the FITS headers and a description of the pixel array (size, bit resolution, image size).
- `preprocess_image.py`: This script locates the solar disk with a solar disk mask and extracts it into an other FITS file for further analysis. Several statistics files are created.
- `detect_sunspots.py`: This script finally locates the sunspots and outputs their position in a JSON file.
  The following two files are helpful to compare with the official sunspot time series.
- `plot_sunspot_monthly_mean.py`: A few computed samples from the year 2000 until now, in comparison with the daily sunspot numbers.
- `plot_sunspot_time_series.py`: The monthly mean over the last 11-year period, from 2009 to 2020.
