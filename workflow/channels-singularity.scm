;; This channel file points to a commit that contains a patch to Guix.
;; It fixes a bug in Guix' packaging of Python that leads to an error
;; when Python is run from a Guix-generated Singularity image.
;;
;; The bug will be fixed in Guix the next time Python and other
;; central packages are updated. This involves recompiling thousands of packages,
;; and therefore is done only occasionally.
;;
;; In the meantime, we rely on this patch as a workaround. We invite you to
;; look at
;;  https://codeberg.org/khinsen/guix/src/branch/graft-fix-python-sitecustomize
;; to convince yourself that it is a minor modification to commit
;; 7b0863f07a113caef26fea13909bd97d250b629e of the main Guix branch, which is
;; what we use in channels.scm.

(list (channel
       (name 'guix)
       (url "https://codeberg.org/khinsen/guix.git")
       (branch "graft-fix-python-sitecustomize")
       (commit
        "615023ddc9407f27ec7c04060fb520a4cbb3b03f")))
