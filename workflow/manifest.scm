;; This file defines the runtime environment for the workflow in Guix.
;; To run the workflow under Guix in a fully isolated environment, do
;;
;;    guix shell -C -N \
;;       --expose=/etc/ssl/certs --expose=/etc/protocols \
;;       -m workflow/manifest.scm \
;;       -- snakemake --snakefile workflow/Snakefile --cores 3 \
;;       intensity_statistics_on_random_sample
;;
;; in the root directory of the repository.
;;
(specifications->manifest
 '("snakemake"
   "git" "git-annex"
   "bash"
   "coreutils"
   "wget"
   "python"
   "python-fitsio"
   "python-numpy"
   "python-matplotlib"
   "python-scikit-image"
   "nss-certs" "openssl" "net-base"))
