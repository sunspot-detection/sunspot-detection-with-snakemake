import sys
_, \
    fits_image_file_name, \
    fits_image_error_log, \
    sunspot_tophat_file_name, \
    sunspot_threshold_file_name, \
    sunspot_marker_file_name, \
    sunspot_list_file_name \
        = sys.argv

import matplotlib.pyplot as plt
import numpy as np

def show_image(image, filename):
    plt.clf()
    plt.imshow(image, cmap='gray', origin='lower')
    plt.colorbar()
    plt.savefig(filename)

def intensity_cross_sections(intensities, filename, threshold=None):
    plt.clf()
    for i in intensities:
        plt.plot(i, '-')
    if threshold is not None:
        plt.plot([0, max(map(len, intensities))],
                 [threshold, threshold],
                 'k-')
    plt.savefig(filename)

#######################################################
# Propagate error messages from the preprocessing step
#######################################################

with open(fits_image_error_log) as input_error_log:
    input_errors = input_error_log.read()
    if input_errors:
        sys.stderr.write("Errors in input file preparation\n")
        sys.stderr.write(input_errors)
        sys.stderr.write("\n")

##################################################
# Load the residual image
##################################################

import fitsio

fits_image_file = fitsio.FITS(fits_image_file_name)
fits_image_file.update_hdu_list()
images = [hdu for hdu in fits_image_file.hdu_list
          if hdu.has_data() and hdu.get_info()['hdutype'] == fitsio.IMAGE_HDU]
assert(len(images) == 1)
residual_disk = images[0].read()

residual_disk_header = images[0].read_header()
x_center = residual_disk_header['x_center']
y_center = residual_disk_header['y_center']
radius = residual_disk_header['radius']

##################################################
# Create a mask for the solar disk
##################################################

import skimage.draw

residual_disk_mask = np.zeros(residual_disk.shape, dtype=np.uint8)
yy, xx = skimage.draw.disk((y_center, x_center), 0.95*radius, shape=residual_disk_mask.shape)
residual_disk_mask[yy, xx] = 1

##################################################
# Find sunspots
##################################################

# The "black tophat" transform highlights dark objects in an image
# while removing intensity variation in the image background. This
# requires a specification of how to distinguish dark objects from
# darker zones of the background. This specification takes the form of
# a "structure element". We use a disk whose radius is 10% of the
# solar disk, thus assuming that sunspots are smaller than this. The
# blackhat transform inverts the sunspots' intensities, showing them
# as bright spots on a black background. In addition to this image, we
# show its intensity profiles with the threshold used in the following
# for defining sunspots. Warning: its choice is highly arbitrary.

import skimage.morphology

circle = skimage.morphology.disk(radius // 10)
tophat = residual_disk_mask * skimage.morphology.black_tophat(residual_disk, circle)
show_image(tophat, sunspot_tophat_file_name)
threshold = 800
intensity_cross_sections([tophat.max(axis=1), tophat.max(axis=0)],
                         threshold=threshold,
                         filename=sunspot_threshold_file_name)

# We use a measuring function that analyzes zones about that threshold
# and groups them into contiguous regions, corresponding (ideally) to
# individual sunspots.

import skimage.measure

peaks = tophat > threshold
sunspot_labels = skimage.measure.label(peaks)
regions = skimage.measure.regionprops(
                sunspot_labels, residual_disk)

# We store the sunspot data in a JSON file for future use.

import json

class SKImageRegionEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        elif isinstance(obj, (np.int_, np.int32, np.int64)):
            return int(obj)
        elif isinstance(obj, (np.float32, np.float64)):
            return float(obj)
        # The class RegionProperties is not documented, so we access it
        # conservatively as the type of an instance. The entry "slice" is
        # not useful enough for complicating the encoder by handling
        # slice objects, so we filter it out. We also filter out the various
        # moments since some of them are undefined for our data.
        elif isinstance(obj, type(regions[0])):
            return {k: obj[k]
                    for k in obj
                    if k != 'slice' and not k.startswith('moments')}
        return json.JSONEncoder.default(self, obj)

with open(sunspot_list_file_name, 'w') as json_file:
    json.dump(regions, json_file, cls=SKImageRegionEncoder)

# Whe show the residual disk with a marker (red circle) for each
# sunspot, plus a big blue circle indicating the limit of the solar
# disk.

plt.clf()
plt.imshow(residual_disk, cmap='gray', origin='lower')
angles = 2.*np.pi*np.arange(200)/200.
plt.plot(radius*np.cos(angles) + x_center,
         radius*np.sin(angles) + y_center,
         color='blue')
for r in regions:
    c = r['centroid']
    plt.plot(c[1], c[0],
             marker = 'o', markeredgecolor='r', markerfacecolor='none', markersize=12)
plt.savefig(sunspot_marker_file_name)
