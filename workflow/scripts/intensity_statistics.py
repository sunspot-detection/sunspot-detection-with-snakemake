import sys
_, threshold, image_file_name, histogram_file_name = sys.argv

threshold = int(threshold)

##################################################
# Load the image from the FITS file
##################################################

import fitsio
import numpy as np
import matplotlib.pyplot as plt

image_file = fitsio.FITS(image_file_name)
image_file.update_hdu_list()
images = [hdu for hdu in image_file.hdu_list
          if hdu.has_data() and hdu.get_info()['hdutype'] == fitsio.IMAGE_HDU]
assert(len(images) == 1)
raw_image = images[0].read()

#######################################################
# Reduce 15-bits-per-pixel images to 12 bits per pixel
#######################################################

bits_used = np.bitwise_or.reduce(raw_image.flat)
if np.bitwise_and(bits_used, 0x1000) != 0:
    raw_image = np.right_shift(raw_image, 3)
    bits_used = np.bitwise_or.reduce(raw_image.flat)
if np.bitwise_and(bits_used, 0x1000) != 0:
    print("Image ", input_file_name, " uses more than 12 bits/pixel")
    raise ValueError()

##################################################
# Scale down larger images to 1024 x 1024
##################################################

if raw_image.shape == (2048, 2048):
    raw_image.shape = (1024, 2, 1024, 2)
    raw_image = np.right_shift(raw_image.sum(1).sum(2), 2)
if raw_image.shape != (1024, 1024):
    print("Image size for ", input_file_name, ": ", raw_image.shape)
    raise ValueError()

##################################################
# Plot the intensity histogram and threshold
##################################################

counts = np.bincount(raw_image.flatten())
plt.clf()
plt.plot(counts)
plt.vlines(threshold, 0, np.max(counts), color='red')
plt.savefig(histogram_file_name)
