import sys
_, \
    solar_disk_threshold, \
    fits_image_file_name, \
    raw_image_file_name, \
    fit_parameter_file_name, \
    intensity_fit_cross_section_x_file_name, \
    intensity_fit_cross_section_y_file_name, \
    intensity_residual_cross_section_file_name, \
    intensity_residual_disk_file_name, \
    intensity_residual_disk_fits_file_name, \
        = sys.argv


solar_disk_threshold = int(solar_disk_threshold)

import matplotlib.pyplot as plt
import numpy as np

def show_image(image, filename):
    plt.clf()
    plt.imshow(image, cmap='gray', origin='lower')
    plt.colorbar()
    plt.savefig(filename)

def intensity_cross_sections(intensities, filename, threshold=None):
    plt.clf()
    for i in intensities:
        plt.plot(i, '-')
    if threshold is not None:
        plt.plot([0, max(map(len, intensities))],
                 [threshold, threshold],
                 'k-')
    plt.savefig(filename)

##################################################
# Load the raw image and plot it for inspection
##################################################

import fitsio

fits_image_file = fitsio.FITS(fits_image_file_name)
fits_image_file.update_hdu_list()
images = [hdu for hdu in fits_image_file.hdu_list
          if hdu.has_data() and hdu.get_info()['hdutype'] == fitsio.IMAGE_HDU]
assert(len(images) == 1)
raw_image = images[0].read()

# The image database contains images of two different sizes (1024x1024
# and 2048x2048) and of different resolutions (12 bit and 15 bit per
# pixel). We normalize to 1024x1024 and 15 bit.

bits_used = np.bitwise_or.reduce(raw_image.flat)
if np.bitwise_and(bits_used, 0x1000) != 0:
    raw_image = np.right_shift(raw_image, 3)
    bits_used = np.bitwise_or.reduce(raw_image.flat)
if np.bitwise_and(bits_used, 0x1000) != 0:
    print("Image ", input_file_name, " uses more than 12 bits/pixel")
    raise ValueError()

if raw_image.shape == (2048, 2048):
    raw_image.shape = (1024, 2, 1024, 2)
    raw_image = np.right_shift(raw_image.sum(1).sum(2), 2)
if raw_image.shape != (1024, 1024):
    print("Image size for ", fits_image_file_name, ": ", raw_image.shape)
    raise ValueError()

show_image(raw_image, raw_image_file_name)

##################################################
# Locate the solar disk
##################################################

# The intensity threshold used to detect the solar disk should correpsond
# to a mininum in the histogram. Print a warning if it doesn't seem to be.

counts = np.bincount(raw_image.flatten())
max_count = counts[solar_disk_threshold-100:solar_disk_threshold+100].max()
if max_count > 100:
    print(f'Warning: many pixels are near the solar disk intensity threshold ({max_count})', file=sys.stderr)

# We take all pixels above the threshold to be inside the solar
# disk. We can then compute its center as the centroid of the surface
# defined by these pixels, and the radius from the radius of gyration
# of the disk.

temp_solar_disk_mask = (raw_image >= solar_disk_threshold).astype(np.uint8)
temp_solar_disk_pixel_count = np.sum(temp_solar_disk_mask)
x_indices = np.arange(raw_image.shape[1])[np.newaxis, :]
y_indices = np.arange(raw_image.shape[0])[:, np.newaxis]
x_center = np.sum(temp_solar_disk_mask * x_indices) / temp_solar_disk_pixel_count
y_center = np.sum(temp_solar_disk_mask * y_indices) / temp_solar_disk_pixel_count
radial_distance_sq = temp_solar_disk_mask * ((x_indices-x_center)**2 + (y_indices-y_center)**2)
radius = np.sqrt(2.*np.sum(radial_distance_sq) / temp_solar_disk_pixel_count)
(x_center,y_center, radius)

# Some functions used later require integer parameters for the solar disk.

x_center = int(round(x_center))
y_center = int(round(y_center))
radius = int(round(radius))

# We make a new perfectly circular mask from the center and radius,
# plus a new solar disk image in which all background pixles are set
# to zero. We reduce the radius a bit in order to avoid noise (due to
# low contrast) near the edge.

radius_scale_factor = 0.95

import skimage.draw
solar_disk_mask = np.zeros(raw_image.shape, dtype=np.uint8)
yy, xx = skimage.draw.disk((y_center, x_center), radius_scale_factor*radius, shape=solar_disk_mask.shape)
solar_disk_mask[yy, xx] = 1
solar_disk = solar_disk_mask*raw_image

##################################################
# Modelling the solar intensity profile
##################################################

# The intensity of the solar disk decreases from the center outwards,
# due to a geometric effect called limb darkening (see
# https://en.wikipedia.org/wiki/Limb_darkening). The resulting
# intensity variation makes it more difficult to identify the
# variation due to sunspots, which happens on a much smaller length
# scale. Therefore we fit a simple model for limb darkening to the
# observed image, and look for sunspots in the difference between the
# image and the model. We follow the derivation in the Wikipedia
# article cited above, using a model of order N=2.

radial_distance = np.sqrt(solar_disk_mask * ((x_indices-x_center)**2 + (y_indices-y_center)**2))
rho = radial_distance / radius
one_minus_cos_psi = solar_disk_mask - np.sqrt(solar_disk_mask*(1-rho**2))

def model(parameters):
    I_0, A_1, A_2 = parameters
    m = I_0 * solar_disk_mask * (1 + one_minus_cos_psi*(A_1 + one_minus_cos_psi*A_2))
    # The model can have negative values for certain input parameters,
    # which in turn can occur when called by optimization algorithms.
    return np.maximum(0., m)

# As starting values for the parameters, we take the intensity maximum
# from the image for I_0, and the values for A_1 and A_2 given
# in the Wikipedia article.

I_0 = solar_disk.max()
A_1 = -0.47
A_2 = -0.23

# For improving these parameters, we define a cost function as the
# squared difference of twice the square root of the observations and
# twice the square root of the prediction, computed over a reduces
# solar disk because of the smaller intensities, and thus larger
# noise, on the outer limb region.

sqrt_solar_disk = np.sqrt(solar_disk)
def residuals(parameters):
    return 2 * (sqrt_solar_disk - np.sqrt(model(parameters)))

cost_mask = solar_disk_mask * (rho <= 0.9).astype(np.int8)
pixels_in_cost_mask = cost_mask.sum()
def cost(parameters):
    diff = cost_mask * residuals(parameters)
    return np.sum(diff**2)/pixels_in_cost_mask

# We use an off-the-shelf optimization algorithm, Nelder-Wead.

from scipy.optimize import minimize
fit = minimize(cost, [I_0, A_1, A_2], method='Nelder-Mead',
               options={'disp': False, 'return_all': True, 'maxiter': 1000})
final_parameters = [fit.x[0], fit.x[1], fit.x[2]]
final_cost = fit.fun
fitted_solar_disk_model = model(final_parameters)

# Model parameters and cost before and after the fit are written to
# the parameter output file.

with open(fit_parameter_file_name, 'w+') as description_file:
    description_file.write("Solar disk (x, y), r: (%d, %d), %d\n\n"
                           % (x_center, y_center, radius))
    description_file.write("Initial parameters: %f, %f, %f\n"
                           % (I_0, A_1, A_2))
    description_file.write("Initial cost: %f\n\n"
                           % cost([I_0, A_1, A_2]))
    description_file.write("Final parameters: %f, %f, %f\n"
                           % tuple(final_parameters))
    description_file.write("Final cost: %f\n\n"
                           % final_cost)

# Plot the fitted model in comparison with the observed intensity profiles

intensity_cross_sections([solar_disk[int(y_center), :],
                          fitted_solar_disk_model[int(y_center), :]],
                         filename=intensity_fit_cross_section_x_file_name)
intensity_cross_sections([solar_disk[:, int(x_center)],
                          fitted_solar_disk_model[:, int(x_center)]],
                         filename=intensity_fit_cross_section_y_file_name)

# The fitted model describes the intensity variation due to the
# geometry of the observation. It's the residuals of the fit that we
# are interested in, because that's where the sunspots are. We
# construct an image from the residuals, which is slightly reduced in
# size (-5% in the radius) because of the imprecisions in the edge of
# the disk resulting from the previous steps.

residual_disk_mask = solar_disk_mask * (rho <= 0.95).astype(np.int8)
residual_disk = residual_disk_mask * (solar_disk - fitted_solar_disk_model)
intensity_cross_sections([residual_disk[int(x_center), :],
                          residual_disk[:, int(y_center)]],
                         filename=intensity_residual_cross_section_file_name)
show_image(residual_disk, intensity_residual_disk_file_name)

# This is the image that will be analyzed in the next step, so we save
# it to a FITS file, along with the data needed to reconstruct the
# solar disk masks. The file is removed before writing new data
# because otherwise the image will be added to the already existing
# file. The center and radius of the solar disk are recorded in the
# FITS header.

import os

try:
    os.remove(intensity_residual_disk_fits_file_name)
except FileNotFoundError:
    pass
with fitsio.FITS(intensity_residual_disk_fits_file_name, 'rw') as residual_image_file:
    residual_image_file.write(residual_disk,
                              header={'x_center': x_center,
                                      'y_center': y_center,
                                      'radius': radius})
