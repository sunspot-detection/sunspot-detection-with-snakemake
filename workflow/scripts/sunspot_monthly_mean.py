import sys
import os
import datetime
import json
import matplotlib.pyplot as plt
import numpy as np

# This script relies on dictionaries maintaining insertion
# order, which is guaranteed only for Python 3.7 or higher.
assert sys.version_info.major > 3 \
       or (sys.version_info.major == 3 and sys.version_info.minor >= 7)

sunspot_count_filename = sys.argv[1]
plot_filename = sys.argv[2]
sunspot_number_filename = sys.argv[3]
sunspot_filenames = sys.argv[4]

##################################################
# Read the computed sunspots
##################################################

# If there were any errors or warnings during the
# preprocessing or sunspot detection steps, we skip
# the image in order to prevent contamination by
# wrong results. This only works if such errors or
# warnings are exceptional.

dates = []
counts = []
for sunspot_filename in open(sunspot_filenames).read().split():
    directory, filename = os.path.split(sunspot_filename)
    basename = filename[:17]
    input_errors = open(os.path.join(directory, basename + '_stderr.log')).read()
    if input_errors:
        sys.stderr.write(f"Skipping image {basename} because of errors:\n")
        sys.stderr.write(input_errors)
        sys.stderr.write("\n")
    else:
        sunspot_data = json.load(open(sunspot_filename.strip(), 'r'))
        counts.append(len(sunspot_data))
        dates.append(datetime.datetime.strptime(basename, 'UPH%Y%m%d%H%M%S'))

##################################################
# Average the sunspot counts over a month
##################################################

def monthly_means(dates, counts):
    mm = {(d.year, d.month): (0, 0) for d in dates}
    for d, c in zip(dates, counts):
        key = (d.year, d.month)
        total, nmonths = mm[key]
        mm[key] = (total+c, nmonths+1)
    mm_dates = []
    mm_counts = []
    for k, v in mm.items():
        if v[1] > 0:
            mm_dates.append(datetime.date(k[0], k[1], 15))
            mm_counts.append(v[0] / v[1])
    return mm_dates, mm_counts

mm_dates, mm_counts = monthly_means(dates, counts)

#########################################
# Write out the averaged sunspot counts
#########################################

with open(sunspot_count_filename, 'w') as output:
    for d, c in zip(mm_dates, mm_counts):
        output.write(d.isoformat())
        output.write(" ")
        output.write(str(c))
        output.write("\n")

############################################################
# Read the monthly means of the official sunspot numbers
# Keep only the points that correspond to the computed ones.
############################################################

sn_dates = []
sns = []
for line in open(sunspot_number_filename, 'r'):
    date = datetime.datetime.strptime(line[:7], '%Y %m').date() \
           + datetime.timedelta(days=15)
    if date >= mm_dates[0] and date <= mm_dates[-1]:
        sn_dates.append(date)
        sns.append(float(line[17:23]))

##################################################
# Generate the comparative plot
#
# The computed sunspot counts are multiplied by
# a factor 15 to match the scale of the official
# sunspot numbers. The factor 15 is purely
# empirical and very approximate.
##################################################

plt.clf()
plt.plot(sn_dates, sns, label='Sunspot Number')
plt.plot(mm_dates, 15.*np.array(mm_counts), label='Scaled sunspot count')
plt.gcf().autofmt_xdate()
plt.legend()
plt.savefig(plot_filename)
